@extends('layouts.app')

@section('title','Connexion')

@push('css')
    <link rel="stylesheet" href="{{ asset('backend/css/style.css') }}">
@endpush

@section('content')
<div class="main-agileinfo slider ">
		<div class="items-group">
			<div class="item agileits-w3layouts">
				<div class="block text main-agileits"> 
					<span class="circleLight"></span> 
					<!-- login form -->
					<div class="login-form loginw3-agile"> 
						<div class="agile-row">
							<h1>Connectez-vous</h1> 
							<div class="login-agileits-top"> 	
								<form method="POST" action="{{ route('login') }}" enctype="multipart/form-data">
                                @csrf
                                    <label class="bmd-label-floating">Email</label>
									<input type="email" class="form-control" name="email" value="{{ old('email') }}" required>
									<label class="bmd-label-floating">Mot de passe</label>
                                    <input type="password" class="form-control" name="password" required>
                                    <br>
                                    <input type="submit" value="Login"> <a href="{{ route('welcome') }}">Retour</a>
                                    <br>
                                    <br>
                                    
                                </form> 	
							</div> 
							
						</div>  
					</div>   
				</div>
				<div class="w3lsfooteragileits">
                <strong style="color:white"> Développé avec <i class="material-icons">favorite</i> par </strong>
                        <a href="https://www.inovtechsenegal.com" target="_blank">Antoine Ngor SARR</a>
				</div> 
			</div>   
		</div>
	</div>
@endsection

@push('scripts')

@endpush