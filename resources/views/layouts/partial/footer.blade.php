<footer class="footer">
        <div class="container-fluid">
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>
            <strong> Développé avec <i class="material-icons">favorite</i> par </strong>
                        <a href="https://www.inovtechsenegal.com" target="_blank">Antoine Ngor SARR</a>
          </div>
        </div>
      </footer>